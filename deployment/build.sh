#!/bin/bash

# Abort on error
set -e

# Build xkeygen debian package
cargo deb --no-strip -p xkeygen

export ARTIFACTORY_ARTIFACTS_URL="https://transparentinc.jfrog.io/artifactory/artifacts-internal"
export CRATE_VER=$(toml get Cargo.toml package.version | tr -d \")
export BINARY_DEB_PACKAGE_NAME=$(basename ./target/debian/xkeygen_$CRATE_VER*.deb)
export PROPERTIES="job_url=${CI_JOB_URL};branch=${CI_COMMIT_REF_NAME};pipeline_id=${CI_PIPELINE_IID}"

if curl -u$ARTIFACTORY_USER:$ARTIFACTORY_PASS --output /dev/null --silent --head --fail $ARTIFACTORY_ARTIFACTS_URL/xkeygen/$BINARY_DEB_PACKAGE_NAME; then
  echo "$BINARY_DEB_PACKAGE_NAME is already published. No publishing happened!!"
else
  curl -u$ARTIFACTORY_USER:$ARTIFACTORY_PASS -T ./target/debian/$BINARY_DEB_PACKAGE_NAME "$ARTIFACTORY_ARTIFACTS_URL/xkeygen/$BINARY_DEB_PACKAGE_NAME;$PROPERTIES"
fi
