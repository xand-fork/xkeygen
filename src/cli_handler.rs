use std::path::{Path, PathBuf};
use tpfs_krypt::{errors::KeyManagementError, XandKeyPair, XandKeyType};

pub(crate) fn generate_print_save<P: AsRef<Path>>(
    key_type: XandKeyType,
    out_dir: P,
) -> Result<PathBuf, KeyManagementError> {
    let key_pair = key_type.generate()?;
    if let Some(address) = key_pair.address() {
        println!("Address: {}", address);
    }
    let key_file_path = key_pair.write_to_directory(out_dir)?;
    println!("Key generated at: {}", key_file_path.display());
    Ok(key_file_path)
}

#[cfg(test)]
mod tests {
    use super::generate_print_save;
    use strum::IntoEnumIterator;
    use tpfs_krypt::XandKeyType;

    fn generation_succeeds(key_type: XandKeyType) {
        let temp_dir = tempfile::TempDir::new().unwrap();
        let saved_path = generate_print_save(key_type, temp_dir.path()).unwrap();
        assert!(
            saved_path.exists(),
            "Generation failed for type {}",
            key_type
        );
    }

    #[test]
    fn generation_succeeds_for_all_key_types() {
        for key_type in XandKeyType::iter() {
            generation_succeeds(key_type);
        }
    }

    #[test]
    fn generation_fails_with_non_existing_dir() {
        let temp_dir = tempfile::TempDir::new().unwrap();
        let test_dir = temp_dir.path().join("idontexist");
        let result = generate_print_save(XandKeyType::Wallet, test_dir);
        assert!(result.is_err());
    }
}
